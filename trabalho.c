#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "mpi.h"

#define VETORES 10000
#define QTD 32000

int **matriz;
int **matriz_recebimento;//[VETORES][QTD];
int **matriz_aux;


//Função utilizada no callback da função qSort nativa do C 
int compara(const void * a, const void * b) {
  return (*(int*)a - *(int*)b );
}

//Função que retorna um inteiro randômico entre um intervalo de números
int RandomInteger(int low, int high){
    int k;
    double d;
    d = (double) rand( ) / ((double) RAND_MAX + 1);
    k = d * (high - low + 1);
    return low + 2*k;
}

int mestre() {
	int j = 0, aux = 0, tag = 50, k = 0, i = 0, feito = 0;
	int my_rank;
	int proc_n;
	int dest;
	
	MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_n);

	//printf("É o mestre: %d\n", my_rank);

	for (i = 0; i < VETORES; i++) {
		matriz[i] = (int *) malloc(QTD*sizeof(int));
		matriz_aux[i] = (int *) malloc(QTD*sizeof(int));
	}

	//Preenchimento da matriz de vetores com números randômicos
	for(aux = 0; aux < VETORES; aux++) {
		for(k = 0; k < QTD; k++) {
			matriz[aux][k] = RandomInteger(111,1000);		
		}
	}

	//Decrementa número de processos
	proc_n--;

	//printf("Value: %d\n", matriz[0][0]);
	//printf("Mestre enviou %d - para o escravo: %d\n", j, 1);
	for(i = 0; i < proc_n; i++) {
		MPI_Send(matriz[j], (8*QTD), MPI_INT, i+1, tag, MPI_COMM_WORLD);
		j = j + 8;
	}

	aux = 0;
	int fim[1] = {-10};
	while(aux < VETORES) {
		MPI_Recv(&matriz_aux[aux], (QTD*8), MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		dest = status.MPI_SOURCE;	
		//printf("Mestre recebeu vetor do escravo %d \n", dest);				
		feito++;				
		
		//printf("Enviando de novo para o escravo %d - Controle: %d - %d \n", dest, aux, j);
				
		aux = aux + 8;
		if((j+8) >= VETORES) { 
			MPI_Send(&fim, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
			break;
		}
			
		MPI_Send(&matriz[j], (QTD*8), MPI_INT, dest, tag, MPI_COMM_WORLD);	
		j = j + 8;			
	}

	
	for(i = 0; i < proc_n; i++) {
		MPI_Send(&fim, 1, MPI_INT, i+1, tag, MPI_COMM_WORLD);
	}			
	
	/* E espera pelos últimos resultados a serem recebidos */
	while(feito < VETORES) {
		//printf("Mestre recebendo vetores de %d do escravo %d: \n", aux, dest);				
		MPI_Recv(&matriz_aux[aux], (QTD*8), MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		dest = status.MPI_SOURCE;
		feito++;
		aux = aux + 8;
	}


	MPI_Finalize();
}

void escravo() {
	int my_rank;  /* Identificador do processo */
    int i = 0, nThread, id;
	int aux = 0;
	int tag = 50;
	MPI_Status status;

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
   
	//printf("É o escravo: %d\n", my_rank);

	for (i = 0; i < VETORES; i++) {
		matriz_recebimento[i] = (int *) malloc(QTD*sizeof(int));
	}

	while(aux != -10) {
		//printf("Escravo %d recebendo um vetor de %d elementos do mestre!\n", my_rank, QTD*8);
		MPI_Recv(&matriz_recebimento[0][0], (QTD*8), MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
		aux = matriz_recebimento[0][0];
	
		if(my_rank == 4) nThread = 7;
		else nThread = 8;

		if(aux != -10) {
			#pragma omp parallel for	
			for(i = 0; i < nThread; i++) {
			  id = omp_get_thread_num();
			  qsort(matriz_recebimento[i], (size_t) QTD, sizeof(int), compara);
			}

			//printf("Escravo: %d está enviando o vetor ordenado!\n", my_rank);			
			MPI_Send(&matriz_recebimento[0], (QTD*8), MPI_INT, 0, tag, MPI_COMM_WORLD);			
		}
	}

	
	//MPI_Finalize();
}


int main(int argc, char** argv) {
    int my_rank;  /* Identificador do processo */
    int proc_n;   /* Número de processos */
    int source;   /* Identificador do proc.origem */
    int dest;     /* Identificador do proc. destino */
    int tag = 50; /* Tag para as mensagens */
	double t1, t2;

    MPI_Status status; /* Status de retorno */
    MPI_Init (&argc , &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_n);

	matriz = (int **) malloc(VETORES*sizeof(int *));
	matriz_recebimento = (int **) malloc(VETORES*sizeof(int *));
	matriz_aux = (int **) malloc(VETORES*sizeof(int *));

	t1 = MPI_Wtime();
	if(my_rank == 0) {
		mestre();
	}
	else {
		escravo();		
	}
	
	t2 = MPI_Wtime();

	printf("Tempo de demora: %1.2f\n", t2-t1);
	fflush(stdout);
	

	return 0;
}
